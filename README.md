# waterPi

Simply connect the water sensor to pins:

* **2** for 5v DC
* **6** for Ground
* **40** for Data (GPIO 21)

Using the `GPIO` Python library, the script reads the state of the water sensor, constantly. As soon as the port goes LOW (0v) it knows that water has been detected. A this point, the script will send an email. When the port goes HIGH (5v - the sensor is dry) the script resets. If water is detected again, another email is sent. Only one email per HIGH to LOW trigger is sent.

![Raspberry Pi Wiring](https://bitbucket.org/addibo_uk/waterpi/raw/c0304b1087421a64369585cb60b1e24f9980cda1/assets/raspberryPi.jpg)

Edit the enclosed script to configure the SMTP email settings (from your ISP) and then run it! make sure your Raspberry Pi is connected to a wireless netwprk and has Internet access.

Stick it in your `/etc/rc.local` file to run on boot.