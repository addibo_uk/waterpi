#!/usr/bin/python
import RPi.GPIO as GPIO
import smtplib
import time

# EMAIL SETUP
smtp_server = "smtp.tools.sky.com"
port = 587
sender_email = "chrismoses2@sky.com"
receiver_email = "chrismoses99@gmail.com"
password = "********"
message = """\
Subject: There's a leak in the loft!

From,
WaterPi"""

# GPIO SETUP
channel = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(channel, GPIO.IN)

def callback(channel):
	if not GPIO.input(channel):
		print("Water detected")
		sendEmail()

def sendEmail():
	server = smtplib.SMTP(smtp_server, port)
	server.ehlo()
	server.starttls()
	server.login(sender_email, password)
	server.sendmail(sender_email, receiver_email, message)
	server.quit()

GPIO.add_event_detect(channel, GPIO.BOTH, bouncetime=300)
GPIO.add_event_callback(channel, callback)

while True:
    time.sleep(1)
